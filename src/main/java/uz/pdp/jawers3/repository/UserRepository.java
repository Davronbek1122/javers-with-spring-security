package uz.pdp.jawers3.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.pdp.jawers3.entity.User;

import java.util.Optional;
import java.util.UUID;

public interface UserRepository extends JpaRepository<User, UUID> {
    Optional<User> findByPhoneNumber(String username);

    boolean existsByPhoneNumber(String phoneNumber);
}

