package uz.pdp.jawers3.repository;

import org.javers.spring.annotation.JaversSpringDataAuditable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import uz.pdp.jawers3.entity.Product;
import java.util.UUID;

@RepositoryRestResource(path = "product")
@JaversSpringDataAuditable
public interface ProductRepo extends JpaRepository<Product, UUID> {
}
