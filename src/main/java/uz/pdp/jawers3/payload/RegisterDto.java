package uz.pdp.jawers3.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class RegisterDto {

    @NotNull
    @Size(min = 4, max = 100)
    private String firstName;

    @NotNull
    @Length(min = 4, max = 100)
    private String lastName;

    @NotNull
    private String phoneNumber;

    @NotNull
    private String password;
}
