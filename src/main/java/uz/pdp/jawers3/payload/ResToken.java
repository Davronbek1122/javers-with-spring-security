package uz.pdp.jawers3.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ResToken {
    private String tokenType = "Bearer ";
    private String token;

    public ResToken(String token) {
        this.token = token;
    }
}
