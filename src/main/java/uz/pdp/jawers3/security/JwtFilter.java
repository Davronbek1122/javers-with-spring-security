package uz.pdp.jawers3.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.filter.OncePerRequestFilter;
import uz.pdp.jawers3.entity.User;
import uz.pdp.jawers3.repository.UserRepository;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.UUID;

public class JwtFilter extends OncePerRequestFilter {
    @Autowired
    JwtProvider jwtProvider;

    @Autowired
    UserRepository userRepository;

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        User user = getUserFromToken(request);
        if (user !=null){
            if (user.isEnabled() &&
                    user.isCredentialsNonExpired()
                    && user.isAccountNonLocked()
                    && user.isAccountNonExpired()){
                UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(
                        user,
                        null,
                        user.getAuthorities()
                );
                SecurityContextHolder.getContext().setAuthentication(authentication);
            }
        }
        filterChain.doFilter(request,response);
    }

    public String getTokenFromRequest(HttpServletRequest request){
        String token = request.getHeader("Authorization");
        return token!=null? token.substring(7) : null;
    }
    public User getUserFromToken(HttpServletRequest request){
        String token = getTokenFromRequest(request);
        if (token!=null
                && jwtProvider.validateToken(token)
        ){
            UUID userId = jwtProvider.getSubjectFromToken(token);

            return userRepository.findById(userId).orElse(null);
        }
        return null;
    }
}
