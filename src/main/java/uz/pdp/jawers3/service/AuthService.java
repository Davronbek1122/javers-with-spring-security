package uz.pdp.jawers3.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import uz.pdp.jawers3.entity.User;
import uz.pdp.jawers3.payload.ApiResponse;
import uz.pdp.jawers3.payload.RegisterDto;
import uz.pdp.jawers3.payload.ResToken;
import uz.pdp.jawers3.payload.SignIn;
import uz.pdp.jawers3.repository.UserRepository;
import uz.pdp.jawers3.security.JwtProvider;

import java.util.Optional;

@Service
public class AuthService {

    @Autowired
    JwtProvider jwtProvider;

    @Autowired
    UserRepository userRepository;

    @Autowired
    AuthenticationManager manager;

    @Autowired
    PasswordEncoder passwordEncoder;

    public ApiResponse login(SignIn signIn) {
        Authentication authenticate = manager.authenticate(
                new UsernamePasswordAuthenticationToken(signIn.getPhoneNumber(), signIn.getPassword()));
        Optional<User> optional = userRepository.findByPhoneNumber(signIn.getPhoneNumber());
        if (optional.isPresent() && optional.get().isEnabled()) {
            String token = jwtProvider.generateToken((User) authenticate.getPrincipal());
            return new ApiResponse("ok", true, new ResToken(token));
        }
        return new ApiResponse("error", false);
    }

    public ApiResponse registerUser(RegisterDto registerDto) {
        boolean exist = userRepository.existsByPhoneNumber(registerDto.getPhoneNumber());
        if (!exist) {
            User user = new User();
            user.setFirstName(registerDto.getFirstName());
            user.setLastName(registerDto.getLastName());
            user.setPhoneNumber(registerDto.getPhoneNumber());
            user.setPassword(passwordEncoder.encode(registerDto.getPassword()));

            userRepository.save(user);

            return new ApiResponse("Otdiz", true);
        }
        return new ApiResponse("Boruu", false);
    }

}
