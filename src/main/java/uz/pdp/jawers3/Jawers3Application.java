package uz.pdp.jawers3;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Jawers3Application {

    public static void main(String[] args) {
        SpringApplication.run(Jawers3Application.class, args);
    }

}
